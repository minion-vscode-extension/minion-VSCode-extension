## Project title
A VSCode plugin for the [minion](https://gitlab.com/CinCan/minion/-/tree/master).

## Motivation
This VSCode plugin is purposed to help in usage of Minion tool.

This project has been created as a course work for Software Project course in University of Oulu.

## Build status
![Build Status](https://gitlab.com/minion-vscode-extension/minion-VSCode-extension/badges/master/pipeline.svg) ![Version](https://img.shields.io/visual-studio-marketplace/v/VSCodeMinionPluginProject.minion?color=GREEN&label=Marketplace) ![Downloads](https://img.shields.io/visual-studio-marketplace/i/VSCodeMinionPluginProject.minion?color=GREEN) 

## Code style
ES6
 
## Screenshots
![Minion_image](https://gitlab.com/minion-vscode-extension/minion-VSCode-extension/-/raw/master/images/hello.png "Usage view")

## Tech/framework used

<b>Built with</b>
- VSCode Extension API
- Debug Adapter Protocol
- webpack
- Axios
- Mocha
- Chai

## Features
 - Syntax hilighting for .rules files
 - Basic debug functionality through trackAPI
    - Adding breakpoints
    - Running code with stops at breakpoints
    - See event variables at runtime

## Installation
Needed packages can be intalled using

```
npm install
```

## Tests
Unit tests can be run using following command

```
npm run tests
```

## How to use?
Open project to VSCode (on project root)

```
code .
```

On VSCode, press F5 to start the extension to a extension development host

If you get error "Specified task cannot be tracked ...", press F5 again.

Now the extension is running and you can start debugging. 

Open a .rules file to the editor. Add breakpoints if you wish. 

Then press Debug File button from right top corner of the editor. 

On debug view, you can continue and stop debugging at any point. 

## Deploying
This extension has been deployed to Visual Studio Marketplace.

A automatic deploying stage has been implemented to the CI/CD.

In order to update the extension, run following command on your local environment

```
npm run standard-version
```

Then push your changes to git and create merge request. 

On the merge commit message, start your first line with following in order to run deploy stage on CI

```
[publish]
```

## Contribute
Contributions are always welcome! 

Please feel free to contribute to the project.

## Credits
Rauli Kaksonen

## License
This project is licensed under the [MIT license](https://gitlab.com/minion-vscode-extension/minion-VSCode-extension/-/blob/master/LICENSE).
