import axios from 'axios';
import * as vscode from 'vscode';

const axiosRetry = require('axios-retry');

let instance = axios.create({});

instance.interceptors.request.use(
  config => {
    let settings = vscode.workspace.getConfiguration('minion');
    config.baseURL =  `${settings.get('address')}:${settings.get('port')}`;
    config.timeout =  1000;
    config.headers = { 'X-API-Key': settings.get('apiKey') };

    return config;
  },
  error => Promise.reject(error)
);

axiosRetry(instance, {
  retries: 10,
  shouldResetTimeout: true,
  retryDelay: (retryCount) => {
    return retryCount * 300; // time interval between retries
  },
  retryCondition: (_error) => true,
});

export async function getRules(): Promise<any> {
  try {
    const response = await instance.get('rules');
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function putPause(state: string): Promise<any> {
  try {
    const response = await instance.put(`pause?value=${state}`);
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function getPause(): Promise<any> {
  try {
    const response = await instance.get('pause');
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function getContexts(path: string): Promise<any> {
  try {
    const response = await instance.get(`contexts/${path}`);
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function getAllEvents(): Promise<any> {
  try {
    const response = await instance.get('events');
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function getEvent(num: number): Promise<any> {
  try {
    const response = await instance.get(`events/${num}`);
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function getLatestEvent(): Promise<any> {
  try {
    const response = await instance.get('events/latest');
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function continueAllEvents(): Promise<any> {
  try {
    const response = await instance.put('events?action=continue');
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function continueEvent(num: number): Promise<any> {
  try {
    const response = await instance.put(`events/${num}?action=continue`);
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function addBreakpoint(path: string, num: number): Promise<any> {
  try {
    const response = await instance.post(`breakpoints/${path}?line=${num}`);
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}

export async function deleteBreakpoint(path: string, num: number): Promise<any> {
  try {
    const response = await instance.delete(`breakpoints/${path}?line=${num}`);
    return response.data;
  } catch (error) {
    console.log('error ' + error);
  }
}