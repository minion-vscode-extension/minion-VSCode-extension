import * as vscode from 'vscode';

let terminal: vscode.Terminal;

let NEXT_TERM_ID = 1;

export function startMinion(rulesFile: string) {
  let settings = vscode.workspace.getConfiguration('minion');
  terminal = vscode.window.createTerminal(`Ext Terminal #${NEXT_TERM_ID++}`);
  terminal.sendText(`minion --threads ${settings.get('threads')} --api ${settings.get('port')}` +
  ` --api-key ${settings.get('apiKey')} -r ${rulesFile} use ${settings.get('command')}`);
  terminal.show();
}


export function stopMinion() {
  terminal.dispose(); 
}
