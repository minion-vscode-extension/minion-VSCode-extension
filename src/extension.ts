'use strict';

import * as vscode from 'vscode';
import { activateMinionDebug } from './activateMinionDebug';


export function activate(context: vscode.ExtensionContext) {

  activateMinionDebug(context);

}

export function deactivate() {
  // nothing to do
}
