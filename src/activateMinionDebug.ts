'use strict';

import * as vscode from 'vscode';
import { WorkspaceFolder, DebugConfiguration, ProviderResult, CancellationToken } from 'vscode';
import { MinionDebugSession } from './minionDebug';
import { FileAccessor } from './minionRuntime';
import * as systemCmd from "./systemCommand";

export function activateMinionDebug(context: vscode.ExtensionContext, factory?: vscode.DebugAdapterDescriptorFactory) {

  context.subscriptions.push(
    vscode.commands.registerCommand('extension.minion-debug.runEditorContents', (resource: vscode.Uri) => {
      let targetResource = resource;
      if (!targetResource && vscode.window.activeTextEditor) {
        targetResource = vscode.window.activeTextEditor.document.uri;
      }
      if (targetResource) {
        vscode.debug.startDebugging(undefined, {
          type: 'minion',
          name: 'Run File',
          request: 'launch',
          program: targetResource.fsPath
        },
        { noDebug: true }
        );
      }
    }),
    vscode.commands.registerCommand('extension.minion-debug.debugEditorContents', (resource: vscode.Uri) => {
      let targetResource = resource;
      if (!targetResource && vscode.window.activeTextEditor) {
        targetResource = vscode.window.activeTextEditor.document.uri;
      }
      if (targetResource) {
        vscode.debug.startDebugging(undefined, {
          type: 'minion',
          name: 'Debug File',
          request: 'launch',
          program: targetResource.fsPath
        });
      }
    }),
    vscode.commands.registerCommand('extension.minion-debug.toggleFormatting', (variable) => {
      const ds = vscode.debug.activeDebugSession;
      if (ds) {
        ds.customRequest('toggleFormatting');
      }
    })
  );

  context.subscriptions.push(vscode.commands.registerCommand('extension.minion-debug.getProgramName', config => {
    return vscode.window.showInputBox({
      placeHolder: "Please enter the name of a rules file in the workspace folder",
      value: "minion.rules"
    });
  }));

  // register a configuration provider for 'minion' debug type
  const provider = new MinionConfigurationProvider();
  context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider('minion', provider));

  // register a dynamic configuration provider for 'minion' debug type
  context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider('minion', {
    provideDebugConfigurations(folder: WorkspaceFolder | undefined): ProviderResult<DebugConfiguration[]> {
      return [
        {
          name: "Dynamic Launch",
          request: "launch",
          type: "minion",
          program: "${file}"
        },
        {
          name: "Another Dynamic Launch",
          request: "launch",
          type: "minion",
          program: "${file}"
        },
        {
          name: "Minion Launch",
          request: "launch",
          type: "minion",
          program: "${file}"
        }
      ];
    }
  }, vscode.DebugConfigurationProviderTriggerKind.Dynamic));

  if (!factory) {
    factory = new InlineDebugAdapterFactory();
  }
  context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory('minion', factory));
  if ('dispose' in factory) {
    context.subscriptions.push(factory);
  }

  // override VS Code's default implementation of the debug hover
  /*
	vscode.languages.registerEvaluatableExpressionProvider('markdown', {
		provideEvaluatableExpression(document: vscode.TextDocument, position: vscode.Position): vscode.ProviderResult<vscode.EvaluatableExpression> {
			const wordRange = document.getWordRangeAtPosition(position);
			return wordRange ? new vscode.EvaluatableExpression(wordRange) : undefined;
		}
	});
	*/
}

class MinionConfigurationProvider implements vscode.DebugConfigurationProvider {

  /**
	 * Massage a debug configuration just before a debug session is being launched,
	 * e.g. add all missing attributes to the debug configuration.
	 */
  resolveDebugConfiguration(folder: WorkspaceFolder | undefined, config: DebugConfiguration, token?: CancellationToken): ProviderResult<DebugConfiguration> {

    // if launch.json is missing or empty
    if (!config.type && !config.request && !config.name) {
      const editor = vscode.window.activeTextEditor;
      if (editor && editor.document.languageId === 'markdown') {
        config.type = 'minion';
        config.name = 'Launch';
        config.request = 'launch';
        config.program = '${file}';
      }
    }

    if (!config.program) {
      return vscode.window.showInformationMessage("Cannot find a program to debug").then(_ => {
        return undefined;	// abort launch
      });
    }

    return config;
  }
}

export const workspaceFileAccessor: FileAccessor = {
  async readFile(path: string) {
    try {
      const uri = vscode.Uri.file(path);
      const bytes = await vscode.workspace.fs.readFile(uri);
      const contents = Buffer.from(bytes).toString('utf8');
      return contents;
    } catch(e) {
      try {
        const uri = vscode.Uri.parse(path);
        const bytes = await vscode.workspace.fs.readFile(uri);
        const contents = Buffer.from(bytes).toString('utf8');
        return contents;
      } catch (e) {
        return `cannot read '${path}'`;
      }
    }
  }
};

class InlineDebugAdapterFactory implements vscode.DebugAdapterDescriptorFactory {

  createDebugAdapterDescriptor(_session: vscode.DebugSession): ProviderResult<vscode.DebugAdapterDescriptor> {
    return new vscode.DebugAdapterInlineImplementation(new MinionDebugSession(workspaceFileAccessor));
  }
}

//TODO: shall be moved somehere?
vscode.debug.registerDebugAdapterTrackerFactory('minion', {
  createDebugAdapterTracker(session: vscode.DebugSession) {
    return {
      onWillStopSession: () => {
        console.log(`Stop Minion`);
        systemCmd.stopMinion();
      }
    };
	  }
});